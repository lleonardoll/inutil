Rails.application.routes.draw do 
		namespace :api, defaults: {format: :json} do 
			resources :comments
			resources :favorites
		end
end