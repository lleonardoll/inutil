require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Inutil
  class Application < Rails::Application
		config.paths['config/routes.rb'] << Rails.root.join('config', 'routes_api.rb')
  end
end
