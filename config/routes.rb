Rails.application.routes.draw do
  
	root 'welcome#index'
	get "/em_breve", to: "welcome#coming", as: :coming
	get "/about", to: "welcome#about", as: :about
	
	resources :comments
	resources :favorites

end
