//= require jquery
//= require js-routes

//= require angular
//= require angular-resource
//= require angular-animate
//= require angular-aria
//= require angular-material
//= require angular-mask
//= require ng-file-upload

//= require api/angular/app
//= require_tree .


$(document).ready(function(){

	$(".button-close-menu").click(function(){
		$(".mdl-layout__obfuscator").removeClass("is-visible");
		$(".mdl-layout__drawer").removeClass("is-visible");
	});	

	$(".mdl-layout__drawer-button").click(function(){
		$(".mdl-layout__obfuscator").addClass("is-visible");
		$(".mdl-layout__drawer").addClass("is-visible");
	});
});