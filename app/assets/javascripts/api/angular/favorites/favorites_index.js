(function() {
  'use strict';

  angular
    .module('app')
    .controller('FavoriteIndex', FavoriteIndex);

  FavoriteIndex.$inject = ['favoriteAPI'];

  function FavoriteIndex(favoriteAPI) {

    var rk = this;
    rk.addRanking = create;
    rk.loadRanking = load;
    rk.comment_id = 0;
    rk.ranking = ranking;

    function load(){
      favoriteAPI.index(function(data){
        rk.comments = data;
      }, function(error){
        console.log("ERROR LOAD FAVORITE->", error);
      })
    }

    function create() {
      $.get("http://ipinfo.io", function(response) {
        rk.favorite = {};
        rk.favorite.comment_id = rk.comment_id;
        rk.favorite.address = response.ip;        

        favoriteAPI.create({favorite: rk.favorite}, function(data){
          ranking();
          load();
        }, function(error){
          console.log("ERROR CREATE FAVORITE->", error);
        });
      }, "jsonp");
    };

    rk.address = "";
    function messagePosition(dataAddress){
      if(rk.address == dataAddress){
        return true;
      }
      else{
        return false;
      }
    };

    function ranking(){
      var params = {comment_id: rk.comment_id};
      favoriteAPI.query(params, function(data){
        rk.rankingValue = data.favorites.length;
      }, function(error){
        console.log("ERROR LOAD FAVORITE->", error);
      })
    }

  }
})();