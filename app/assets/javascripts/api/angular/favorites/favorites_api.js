(function() {
  'use strict';

  angular
    .module('app')
    .factory('favoriteAPI', favoriteAPI);

  favoriteAPI.$inject = ['$resource'];

  function favoriteAPI($resource){
    return $resource('', {id: '@id'}, {
      index: {url: '/api/favorites', method: 'GET', isArray: true},
      // new: {url: '/api/users/new', method: 'GET'},
      create: {url: '/api/favorites', method: 'POST'},
      // edit: {url: '/api/users/:id/edit', method: 'GET'},
      // update: {url: '/api/users/:id', method: 'PATCH'},
      // destroy: {url: '/api/users/:id', method: 'DELETE'}
      query: {url: '/api/favorites', method: 'GET'}
    });
  }
})();
