(function() {
  'use strict';

  angular
    .module('app')
    .controller('CommentIndex', CommentIndex);

  CommentIndex.$inject = ['commentAPI'];

  function CommentIndex(commentAPI) {

    var vm = this;
    vm.load = load;
    vm.create = create;
    vm.count = 0;
    vm.comment_id = 0;
    vm.messagePosition = messagePosition;
    vm.getAddress = getAddress;

    function load(){
      getAddress();
      var params = {count: vm.count};
      commentAPI.index(params, function(data){
        vm.comments = data;
      }, function(error){
        console.log("ERROR LOAD COMMENT->", error);
      });
    }

    function create() {
      $.get("http://ipinfo.io", function(response) {
        vm.comment.address = response.ip;

        commentAPI.create({comment: vm.comment}, function(data){
          load();
        }, function(error){
          console.log("ERROR CREATE COMMENT->", error);
        });
      }, "jsonp");
    };

    vm.address = "";
    function messagePosition(dataAddress){
      if(vm.address == dataAddress){
        return true;
      }
      else{
        return false;
      }
    };
    
    var validation = false;
    function getAddress(){
      $.get("http://ipinfo.io", function(response) {
        vm.address = response.ip;

        if(validation == false){
          validation = true;
          load();
        }
      }, "jsonp");
    };
  }
})();
