(function() {
  'use strict';

  angular
  .module('app')
  .factory('commentAPI', commentAPI);

  commentAPI.$inject = ['$resource'];

  function commentAPI($resource){
    return $resource('', {id: '@id'}, {
      index: {url: '/api/comments', method: 'GET', isArray: true},
      // new: {url: '/api/users/new', method: 'GET'},
      create: {url: '/api/comments', method: 'POST'}
      // edit: {url: '/api/users/:id/edit', method: 'GET'},
      // update: {url: '/api/users/:id', method: 'PATCH'},
      // destroy: {url: '/api/users/:id', method: 'DELETE'}
    });
  }
})();
