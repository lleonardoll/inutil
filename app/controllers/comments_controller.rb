class CommentsController < ApplicationController

	def index
		@comment = Comment.new
	end

	def create
		@comment = Comment.new(comment_params)
		@comment.address = request.remote_ip

		if verify_recaptcha(model: @comment) && @comment.save
			redirect_to comments_path
		else
			render :index
		end
	end

	private
	def comment_params
		params.require(:comment).permit(:name, :content, :address)
	end

end