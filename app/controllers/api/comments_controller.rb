class API::CommentsController < API::APIController

	def index
		@count = params[:count]
		@comment_id = params[:comment_id]

		if @count
			@comments = Comment.last(@count).reverse
			render json: @comments, status: :ok
		elsif @comment_id
			@comment = Comment.find(@comment_id)
			render json: @comment.favorites.count, status: :ok
		else
			render json: Comment.all.reverse, status: :ok
		end
		
	end

	def create
		@comment = Comment.new(comment_params)

		if @comment.save
			render json: @comment, status: :ok
		else
			render json: @comment.erros, stauts: :no_conttent
		end
	end

	private
	def comment_params
		params.require(:comment).permit(:name, :content, :address)
	end

end