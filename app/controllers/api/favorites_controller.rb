class API::FavoritesController < API::APIController

	def index
		@comment_id = params[:comment_id]
		if @comment_id
			@comments = Comment.find params[:comment_id]
			render json: @comments, include: :favorites, status: :ok
		else
			@comments = Comment.all
			render json: @comments, include: :favorites, status: :ok
		end
	end

	def create
		@favorite = Favorite.new(favorite_params)
		@favorite.address = request.remote_ip

		if @favorite.save
			render json: @favorite, as: :ok
		else
			render json: @favorite.errors, as: :no_content
		end
	end

	private
	def favorite_params
		params.require(:favorite).permit(:address, :comment_id)
	end

end