class Favorite < ApplicationRecord
	belongs_to :comment
	validates_uniqueness_of :address, scope: :comment_id
end
