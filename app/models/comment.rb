class Comment < ApplicationRecord
	validates_presence_of :name, :content
	has_many :favorites
end
