class CreateFavorites < ActiveRecord::Migration[5.0]
  def change
    create_table :favorites do |t|
      t.string :address
      t.integer :comment_id

      t.timestamps
    end
  end
end
